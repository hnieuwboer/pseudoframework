package org.pseudo.api.framework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

public class Container {

	private static final Comparator<PrioritizedTask> comparator = new Comparator<PrioritizedTask>() {
		@Override
		public int compare(PrioritizedTask pt1, PrioritizedTask pt2) {
			return pt1.getPriority().getIndex() - pt2.getPriority().getIndex();
		}
	};
	private final List<Task> tasks;
	private final PriorityQueue<PrioritizedTask> queue;
	
	public Container() {
		tasks = Collections.synchronizedList(new ArrayList<Task>());
		queue = new PriorityQueue<>(5, comparator);
	}
	
	public void submit(final Task... t) {
		for(final Task t1 : t) {
			if(t1 instanceof PrioritizedTask) {
				queue.add((PrioritizedTask) t1);
			} else {
				tasks.add(t1);
			}
		}
	}
	
	public void revoke(final Task... t) {
		tasks.removeAll(Arrays.asList(t));
	}
	
	public Task getNextAvailable() {
		Iterator<PrioritizedTask> it = queue.iterator();
		while(it.hasNext()) {
			final PrioritizedTask pt = it.next();
			if(pt.activate()) {
				return pt;
			}
		}
		return tasks.size() > 0 ? tasks.remove(0) : null;
	}
	
}
