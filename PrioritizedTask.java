package org.pseudo.api.framework;

public interface PrioritizedTask extends ConditionalTask {
	public Priority getPriority();
}
