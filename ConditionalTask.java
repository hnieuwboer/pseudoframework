package org.pseudo.api.framework;

public interface ConditionalTask extends Task {
	public boolean activate();
}
